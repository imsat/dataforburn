<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Project\ProjectService;



function buildExcelFile(){

    define('EXCEL_OUTPUT_DIR', "target/");

    $reader = new Csv();
    $reader->setInputEncoding('UTF-8');
    $reader->setDelimiter(';');
    $reader->setEnclosure('');
    $reader->setSheetIndex(0);
    $spreadsheet=$reader->load(EXCEL_OUTPUT_DIR."pi_objectif_table.csv");
    $spreadsheet->getActiveSheet()->setTitle("OBJECTIFS_TABLE"); 
    $reader->setSheetIndex(1);
    $spreadsheet=$reader->loadIntoExisting(EXCEL_OUTPUT_DIR."feature_table.csv", $spreadsheet);
    $spreadsheet->getActiveSheet()->setTitle("FEATURES_TABLE");
    $reader->setSheetIndex(2);
    $spreadsheet=$reader->loadIntoExisting(EXCEL_OUTPUT_DIR."us_table.csv", $spreadsheet);
    $spreadsheet->getActiveSheet()->setTitle("US_TABLE");
    $reader->setSheetIndex(3);
    $spreadsheet=$reader->loadIntoExisting(EXCEL_OUTPUT_DIR."task_table.csv", $spreadsheet);
    $spreadsheet->getActiveSheet()->setTitle("TASKS_TABLE");


    $writer = new Xlsx($spreadsheet);
    $writer->save(EXCEL_OUTPUT_DIR.'BurnData.xlsx');

}

function findPIObjectifsByItVersion($projectId, $version){

    $versionsFound = array();       
   
    if(str_starts_with($version,"PI")){

        $projectService = new ProjectService();

        $versions = $projectService->getVersions($projectId);           
    
        foreach($versions as $currentVersion){           
    
            if(strpos($currentVersion->name, $version) > -1 )
                $versionsFound[] = $currentVersion->name;
    
        }       
        
        return $versionsFound;

    }
    else{

        $numberPI = substr($version, 0, strpos($version,"."));

        return findPIObjectifsByItVersion($projectId, 'PI'.$numberPI);

    }

}

function lookUpVersion($versions, $versionToFind){

    foreach($versions as $version){

        if(strcmp($versionToFind, $version->name) === 0)

            return true;

    }
    return false;
}

function isBVObjectif($version){

    if(str_contains($version, 'BV'))
        return true;

    return false;
}

function getBVObjectif($issue){

    foreach($issue->fields->fixVersions as $version){

        if(isBVObjectif($version->name))
            return $version->name;
    }

    return 0;
}

function findPIObjectiveByIssues($issues, $version, $piObjectifs){

    $piObjectifsToKeep = array();    

    if(!str_starts_with($version, "PI")){

        foreach($issues as $issue){     

            $res1 = lookUpVersion($issue->fields->fixVersions, $version);
            
            foreach($piObjectifs as $piObjectif){
            
            $res2 = lookUpVersion($issue->fields->fixVersions, $piObjectif);


            if($res1 && $res2)
                $piObjectifsToKeep[$issue->key] = array($piObjectif, isStoryClosed($issue) ? 1 : 0);
            
            }

        }
    }
    else{

        foreach($issues as $issue){ 
            
            foreach($piObjectifs as $piObjectif){
            
            $res2 = lookUpVersion($issue->fields->fixVersions, $piObjectif);

            if($res2)
                $piObjectifsToKeep[$issue->key] = array($piObjectif, isStoryClosed($issue) ? 1 : 0);
            
            }

        }

    }

    return $piObjectifsToKeep;

}

function dataForPITable($piObjectifsToKeep){

    $data = array();

    foreach($piObjectifsToKeep as $key=>$value){

        $old_value = 0;
        $old_key = 0;

        if(isset($data[$value[0]])){
           
            $old_value = $data[$value[0]]["CLOSED_FEATURES"];
            $old_key = $data[$value[0]]["TOTAL_FEATURES"];
        }
       

        $data[$value[0]] = array("CLOSED_FEATURES"=>$old_value+$value[1], "TOTAL_FEATURES"=>$old_key+1);
    
       
    }

    return $data;

}


function isEnablerStory($issue){

    if(strcmp($issue->fields->issuetype->name, "Enabler Story") === 0)
        return "OUI";
    return "NON";
}

function strtrimandlowered($str){

    return mb_strtolower(trim($str),'UTF-8');
}

function isStoryClosed($issue){ 

    if(strcmp(strtrimandlowered($issue->fields->status->name), strtrimandlowered("TERMINÉ")) === 0)
        return 1;

    return 0;
}

function countClosedTaskForIssue($issue){

    $i=0;
    
    foreach($issue->fields->subtasks as $task){
      
        if(isStoryClosed($task))
            $i++;

    }

    return $i;


}

function countClosedIssues($issues){

    $i=0;
    
    foreach($issues as $issue){
      
        if(isStoryClosed($issue))
            $i++;

    }

    return $i;
}

function version_cible($version){

    return " ".$version;
}

function objectif_record($issue_key, $value, $version_cible){

    $projectService = new ProjectService();

    $issue_key_parts = explode("-", $issue_key);

    $businessValue = substr($issue_key_parts[1], strlen("BV"));
   
    $jiraVersion = $projectService->getVersion('FSBY', $issue_key);      

    $line['#'] = $jiraVersion->name;
    $line['Version Cible'] = version_cible($version_cible);
    $line['Business Value'] = $businessValue;
    $line['Sujet'] = toUTF8($jiraVersion->description);    
    $line['IS_CLOSED'] = 0;
    $line['TOTAL TASKS'] = $value['TOTAL_FEATURES'];
    $line['CLOSED TASKS'] = $value['CLOSED_FEATURES'];
    $line['AVANCEMENT'] = $line['TOTAL TASKS'] > 0 ? round($line['CLOSED TASKS']/$line['TOTAL TASKS'],2,PHP_ROUND_HALF_UP)*100 : 0;

    return $line;

}



function feature_record($feature, $stories, $version){    
    
    $total_tasks = count($stories);
    $closed_tasks = countClosedIssues($stories);

    return [
        '#' => $feature->key,
        'Version Cible' => version_cible($version),
        'Tâche parente' => getBVObjectif($stories[0]),
        'Fermé' => $feature->fields->resolutiondate != null ? date_format(new DateTime($feature->fields->resolutiondate), 'd/m/y') : null,
		'Code'=> "",
        'Sujet'=> toUTF8($feature->fields->summary),       
        'IS_CLOSED'=>isStoryClosed($feature),
        'TOTAL TASKS'=> $total_tasks,
        'CLOSED TASKS'=> $closed_tasks,
        'AVANCEMENT'=>round($closed_tasks/$total_tasks, 2, PHP_ROUND_HALF_UP)*100
    ];

}

function us_record($issue, $version){

    $total_tasks= property_exists($issue->fields, "subtasks") ? count($issue->fields->subtasks) : 0;
    $closed_tasks=countClosedTaskForIssue($issue);

    return [
        '#' => $issue->key,
        'Version Cible' => version_cible($version),
        'Tâche parente' => property_exists($issue->fields, "customfield_10014") ? $issue->fields->customfield_10014 : null,
        'Enabler' => isEnablerStory($issue),
        'Points de scénarios' => property_exists($issue->fields, "customfield_10024") ? $issue->fields->customfield_10024 : null,
        'Fermé'=> $issue->fields->resolutiondate != null ? date_format(new DateTime($issue->fields->resolutiondate), 'd/m/y') : null,
        'Sujet'=> toUTF8($issue->fields->summary), 
        'IS_CLOSED'=>isStoryClosed($issue),
        'TOTAL TASKS'=>$total_tasks,
        'CLOSED TASKS'=>$closed_tasks,
        'AVANCEMENT'=>$total_tasks > 0 ? round($closed_tasks/$total_tasks, 2, PHP_ROUND_HALF_UP)*100 : 0

    ];

}


function findUsIdByFeatureId($issues){

    $idsFeature = array();

    foreach($issues as $issue){

        if(property_exists($issue->fields, "customfield_10014"))
            $idsFeature[$issue->key]= $issue->fields->customfield_10014;

    }

    foreach($idsFeature as $key=>$value){

        $data[$value][]=$key;
    }

    return $data;

}


function task_record($issue, $version){   

    $estimated_time = 0;
    $reliquat = 0;

    if(property_exists($issue->fields, "progress")){
       
        if(!empty($issue->fields->progress)){

            $progress = json_decode(json_encode($issue->fields->progress), FALSE);
            $estimated_time = property_exists($progress, "total") ? $progress->total/3600 : null;
            $reliquat = property_exists($progress, "percent") ? ($progress->total/3600)-round((($progress->total)*$progress->percent/100)/3600, 2, PHP_ROUND_HALF_UP) : null; 
            $fermeture = $issue->fields->resolutiondate != null ? date_format(new DateTime($issue->fields->resolutiondate), 'd/m/y') : null;
        }

    }     
    
    return [
        '#' => $issue->key,
        'Version Cible' => version_cible($version),
        toUTF8('Tâche parente') => $issue->fields->parent->key,
        toUTF8('Temps estimé') => $estimated_time,
        toUTF8('Restant à faire (heures)') => trim($reliquat),
        toUTF8('Fermé')=> $fermeture,
        'Sujet'=> toUTF8($issue->fields->summary)      

    ];

}

function toUTF8($string){

    //return mb_convert_encoding($string,'HTML-ENTITIES','UTF-8');
    return $string;
}


function hasSubTasks($issue){

    if(property_exists($issue->fields,"subtasks")){
        if(count($issue->fields->subtasks) > 0)
            return true;
    }
        

    return false;
}

function findSubTasks($issue){

    if(hasSubTasks($issue)){

        return $issue->fields->subtasks;
        
    }

    return array();
}


function buildTaskTable($issues, $version){

    $tasks = array();
    $jira = new IssueFinderImpl();

    foreach($issues as $issue){       
        
       foreach(findSubTasks($issue) as $task){           
            
            //ATTENTION le findById de Jira est important car 
            //les objets retournés par findSubTasks sont incomplets
            //Il s'agit d'une limitation de Jira !!!
            $tasks[] = task_record($jira->findById($task->key), $version);   
       }
    
    }

    return $tasks;

}

function buildUsTable($issues, $version){

    $usStories = array();

    foreach($issues as $issue){
       
        $usStories[] = us_record($issue, $version);    
    
    }

    return $usStories;

}

function buildFeatureTable($issues, $version){

    $jira = new IssueFinderImpl();
    $feature_ids = findUsIdByFeatureId($issues);
    $features = array();

    foreach($feature_ids as $feature_id=>$stories_id){

        $link_stories=array();

        foreach($stories_id as $story_id){

            $link_stories[] = $jira->findById($story_id);
        }

        $features[] = array($jira->findById($feature_id), $link_stories);      
    }

    $feature_table = array();

    foreach($features as $feature){
       
        $feature_table[] = feature_record($feature[0], $feature[1], $version);    
    
    }

    return $feature_table;
}

function buildObjectiveTable($issues,$IT, $versions){    
    
    $piobjectifs = findPIObjectiveByIssues($issues, $IT, $versions);

    $objectives = array();

    foreach(dataForPITable($piobjectifs) as $issue_key=>$value){
       
        $objectives[] = objectif_record($issue_key, $value, $IT);    
    
    }

    return $objectives;
}

function formatToCSV($data){  

    $line = implode(";", array_keys($data[0])).PHP_EOL;

    foreach($data as $value){

        $line .= implode(";", $value).PHP_EOL;
        
    }

    return $line;

}

class IssueFinderImpl {
    
    private $issueService;

    public function __construct(){

        $this->initService();

    }

    private function initService(){

        try {
           
            $this->issueService = new IssueService();            
            
        } catch (JiraRestApi\JiraException $e) {
            print("Error Occured! " . $e->getMessage());
        }
    }

    public function findByProjectId($projectId, $limit = 1000){

        $jql = 'project in ('.$projectId.')';

        return $this->issueService->search($jql,0, $limit)->issues;

    }

    public function findLinkedEpicByProjectId($projectId, $limit = 1000){

        $jql = 'project in ('.$projectId.') AND "Epic Link" is not EMPTY';       

        return $this->issueService->search($jql,0, $limit)->issues;

    }

    public function findByLabel($label){


    }

    public function findByEpicLinkId(){

        
    }

    public function findById($id){  

        return $this->issueService->get($id);
    }

    public function findAll(){
        throw new Exception("Not Implemented");
    }

    public function findByVersions($projectId, array $versions){

        $issuesForVersion = array();

        foreach($versions as $version){

            array_push($issuesForVersion, $this->findByVersion($projectId, $version));
        }

        return array_merge(...$issuesForVersion);

    }

    public function findByVersion($projectId, $version){

        $jql = 'project in ('.$projectId.') AND fixVersion in ('.$version.')';       

        return $this->issueService->search($jql,0, 100)->issues;
      
    }

    public function findByVersionForPI($projectId, $version, $objectives){

        if(empty($objectives))
            return array();

        $jql = 'project in ('.$projectId.') AND fixVersion ='.$version.' AND fixVersion in ('.$objectives.')';       

        //$jql = 'project in ('.$projectId.') AND fixVersion in ('.$version.','.$objectives.')';       

        return $this->issueService->search($jql,0, 100)->issues;

    }

}     

$jira = new IssueFinderImpl();
$projectService = new ProjectService();
$PROJECT_ID="FSBY";
$IT=$argv[1];

$versions = findPIObjectifsByItVersion($PROJECT_ID, $IT);

if(str_starts_with($IT,"PI")){
    $issues = $jira->findByVersion($PROJECT_ID, implode(", ", $versions));
}
else{
    $issues = $jira->findByVersionForPI($PROJECT_ID, $IT, implode(", ", $versions));
}

$rapport["pi_objectif_table.csv"] = buildObjectiveTable($issues, $IT, $versions);

$rapport["feature_table.csv"] = buildFeatureTable($issues, $IT);

$rapport["us_table.csv"] = buildUsTable($issues, $IT);

$rapport["task_table.csv"] = buildTaskTable($issues, $IT);


foreach($rapport as $filename=>$value){

    file_put_contents("target/".$filename, formatToCsv($value));

}

buildExcelFile();

/*foreach($rapport as $filename=>$value){

    unlink("target/".$filename);
}*/